﻿using eSimulation.Framework.Service.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace eSimulation.Framework.Service
{
    public class GSTService : IGSTService
    {
        private readonly eSimulationContext _context;
        public GSTService(eSimulationContext context)
        {
            _context = context;
        }
        public int CheckDuplicateGstPanReg(int UserId, int PanId)
        {
            int k;
           if( _context.GSTRegistrations.Any(c =>  c.UserId == UserId && c.Pan_Id ==PanId))
            {
                 k = 1;
                return k;
            }
            else
            {
                k = 0;
                return 0;
            }
        }

       
    }
}
