﻿using eSimulation.Framework.Domain;
using eSimulation.Framework.Service.Core;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace eSimulation.Framework.Service
{
   public class PanService : IPanService
    {
        private readonly eSimulationContext _context;

        public PanService(eSimulationContext Context)
        {
            _context = Context;
        }
        public List <Pannumber> GetAllPan()
        {
            var db = _context.Pannumbers.ToList();
            return db;
        }




    }
}
