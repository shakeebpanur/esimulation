﻿using eSimulation.Framework.Domain;
using eSimulation.Framework.Service.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace eSimulation.Framework.Service
{
   public  class UserService:   IUserService
    {
        private readonly eSimulationContext _dbcontext;

        public UserService(eSimulationContext dbContext)
        {
            _dbcontext = dbContext;
        }
        public User UserLogin(string EmailAddress, string Password)
        {
            var _user = _dbcontext.Users
                .FirstOrDefault(c => c.EmailAddress == EmailAddress && c.Password == Password && c.UserType == Enum.UserType.Student);

            return _user;
        }

        public User AdminLogin(string EmailAddress, string Password)
        {
            //Get Users
            var _user = _dbcontext.Users
                .FirstOrDefault(c => c.EmailAddress == EmailAddress && c.Password == Password && c.UserType == Enum.UserType.Administrator);

            return _user;
        }



    }
}
