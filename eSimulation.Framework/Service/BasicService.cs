﻿using eSimulation.Framework.Domain.Basic;
using eSimulation.Framework.Service.Core;
using eSimulation.Framework.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace eSimulation.Framework.Service
{
    public class BasicService : IBasicService
    {
        private readonly eSimulationContext _context;

        public BasicService(eSimulationContext context)
        {
            _context = context;
        }

        public List<DropdownView> GetStates(int CountryId)
        {
            var _states = _context.States.Where(c => c.CountryId == CountryId)
                .Select(s => new DropdownView
                {
                    Id = s.StateId,
                    Name = s.Name
                })
                .ToList();
            return _states;
        }

        public List<DropdownView> GetDistrict(int StateId)
        {
            var _districts = _context.Districts.Where(c => c.StateId == StateId)
                .Select(s => new DropdownView
                {
                    Id = s.DistrictId,
                    Name = s.Name
                })
                .ToList();
            return _districts;
        }
        public List<Country> GetCountries()
        {
            return _context.Countries.Where(c => c.CountryId == 101).ToList();
        }
    }
}
