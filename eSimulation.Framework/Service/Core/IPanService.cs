﻿using eSimulation.Framework.Domain;
using System;
using System.Collections.Generic;
using System.Text;

namespace eSimulation.Framework.Service.Core
{
    public interface IPanService
    {
        List<Pannumber> GetAllPan();
        
    }
}