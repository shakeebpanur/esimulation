﻿using eSimulation.Framework.Domain;
using System;
using System.Collections.Generic;
using System.Text;

namespace eSimulation.Framework.Service.Core
{
    public interface IGSTService
    {

        //check whether gst is registering with already used pan
       
        int CheckDuplicateGstPanReg(int UserId, int PanNo);
    }
}