﻿using eSimulation.Framework.Domain;
using System;
using System.Collections.Generic;
using System.Text;

namespace eSimulation.Framework.Service.Core
{
    public interface IUserService
    {
        User UserLogin(string Email, string Password);
        User AdminLogin(string Email, string Password);
    }
}
