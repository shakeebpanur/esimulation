﻿using eSimulation.Framework.Domain.Basic;
using eSimulation.Framework.ViewModel;
using System;
using System.Collections.Generic;
using System.Text;

namespace eSimulation.Framework.Service.Core
{
    public interface IBasicService
    {
        List<DropdownView> GetStates(int CountryId);
        List<DropdownView> GetDistrict(int StateId);
        List<Country> GetCountries();
    }
}
