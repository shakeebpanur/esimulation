﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using eSimulation.Framework.Domain;

namespace eSimulation.Framework.Domain
{
    [Table("Pannumber")]

    public class Pannumber
    {

        [Key]
        public int Pan_Id { get; set; }
        
        [Display(Name = "PAN Number")]
        [Required(ErrorMessage = "PAN Required!")]
        [RegularExpression(@"^\(?([A-Z]{5}[0-9]{4}[A-Z]{1})$",ErrorMessage = "Invalid PAN Format")]
        public string pan_no { get; set; }

        [Display(Name = "Temp. Number")]
        [Range(100, 1000000,ErrorMessage ="Minimum 3 digits")]
        public string temporary_no { get; set; }


       



    }

}

