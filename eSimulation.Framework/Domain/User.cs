﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using eSimulation.Framework.Enum;
using eSimulation.Framework.Domain;

namespace eSimulation.Framework.Domain
{
    [Table("User")]

    public class User
    {
       
            [Key]
            public int UserId { get; set; }

            [Required]
            [Display(Name = "User Name")]
            public string Name { get; set; }

            [EmailAddress]
            [Required]
            [Display(Name = "Email Address")]
            public string EmailAddress { get; set; }

            [RegularExpression(@"^[1-9]{1}[0-9]{9}$", ErrorMessage = "Invalid mobile number,It should be 10 digits")]
            public string Mobile { get; set; }

            [Required]
            public string Password { get; set; }
            public UserType UserType { get; set; }




    }

    }

