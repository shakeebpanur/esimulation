﻿using eSimulation.Framework.Enum;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace eSimulation.Framework.Domain
{
    [Table("GSTRegistration")]
    public class GSTRegistration
    {
        [Key]
        public int Reg_Id { get; set; }
        
        public int UserId { get; set; }
        public TaxPayerType TaxpayerType { get; set; }

        [Display(Name = "Country")]
        [Required]
        public int CountryId { get; set; }

        [Display(Name = "State")]
        [Required]
        public int StateId { get; set; }

        [Required]
        [Display(Name = "District")]
        public int? DistrictId { get; set; }

        [Required]
       
        public string Legal_name { get; set; }

        [ForeignKey("Pannumber")]
        public int Pan_Id { get; set; }

        public GstRegStatus GstRegStatus { get; set; }


        [NotMapped]
        [Display(Name = "PAN Number")]
        [Required(ErrorMessage = "PAN Required!")]
        [RegularExpression(@"^\(?([A-Z]{5}[0-9]{4}[A-Z]{1})$", ErrorMessage = "Invalid PAN Format")]
        public string Pan_number { get; set; }


        [EmailAddress]
        [Required]
        public string Email { get; set; }

        [Required]
        [RegularExpression(@"^[1-9]{1}[0-9]{9}$", ErrorMessage = "Invalid mobile number,It should be 10 digits")]
        public string Mobile { get; set; }

        public virtual User User { get; set; }
        public virtual Pannumber Pannumber { get; set; }

        [NotMapped]
        public string Otp { get; set; }

        public DateTime CreatedUtc { get; set; }
        public DateTime ModifiedUtc { get; set; }
        public DateTime ExpiryUtc { get; set; }
        public string FormNumber { get; set; }
        public string FormDescription { get; set; }




    }
}
