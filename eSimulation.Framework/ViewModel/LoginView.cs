﻿using eSimulation.Framework.Enum;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eSimulation.Framework.VIewModel
{
    public class LoginView
    {
        [Required]
        [Display(Name = "Email")]
        public string EmailAddress { get; set; }
        [Required]
        [Display(Name = "Password")]
        public string Password { get; set; }
        public bool Remember { get; set; }
       
    }
}
