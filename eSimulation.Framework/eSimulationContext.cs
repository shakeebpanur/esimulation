﻿using Microsoft.EntityFrameworkCore;
using System;
using eSimulation.Framework.Domain;
using eSimulation.Framework.Domain.Basic;

namespace eSimulation.Framework
{
    public class eSimulationContext:DbContext
    {
        public eSimulationContext(DbContextOptions options) : base(options)
        {

        }
        public DbSet<User> Users { get; set; }
        public DbSet<Pannumber> Pannumbers { get; set; }
        public DbSet<Country> Countries { get; set; }
        public DbSet<State> States { get; set; }
        public DbSet<District> Districts { get; set; }
        public DbSet<GSTRegistration> GSTRegistrations { get; set; }

    }
}
