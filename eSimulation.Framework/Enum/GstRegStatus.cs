﻿using System;
using System.Collections.Generic;
using System.Text;

namespace eSimulation.Framework.Enum
{
    public enum GstRegStatus
    {
            Draft=1,
            Active,
            Inactive
    }
}
