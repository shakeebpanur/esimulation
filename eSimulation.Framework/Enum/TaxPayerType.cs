﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace eSimulation.Framework.Enum
{
    public enum TaxPayerType
    {
        [Display(Name = "Tax Deductor")]
        TaxDeductor =1,
        [Display(Name = "Tax Collector (E-Commerce)")]
        TaxCollectorECommerce,
        [Display(Name = "GST Practitioner")]
        GSTPractitioner,
        [Display(Name = "Non Resident Taxable Person")]
        NonResidentTaxablePerson,
        [Display(Name = "United Nation Body")]
        UnitedNationBody,
        [Display(Name = "Consulate Or Embassy Of Foreign Country")]
        ConsulateOrEmbassyOfForeignCountry,
        [Display(Name = "Other Notified Person")]
        OtherNotifiedPerson,
        [Display(Name = "Non Resident Online Services Provider")]
        NonResidentOnlineServicesProvider,
    }
}
