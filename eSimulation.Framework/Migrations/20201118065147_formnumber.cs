﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace eSimulation.Framework.Migrations
{
    public partial class formnumber : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTime>(
                name: "CreatedUtc",
                table: "GSTRegistration",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<DateTime>(
                name: "ExpiryUtc",
                table: "GSTRegistration",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "FormDescription",
                table: "GSTRegistration",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "FormNumber",
                table: "GSTRegistration",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "ModifiedUtc",
                table: "GSTRegistration",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CreatedUtc",
                table: "GSTRegistration");

            migrationBuilder.DropColumn(
                name: "ExpiryUtc",
                table: "GSTRegistration");

            migrationBuilder.DropColumn(
                name: "FormDescription",
                table: "GSTRegistration");

            migrationBuilder.DropColumn(
                name: "FormNumber",
                table: "GSTRegistration");

            migrationBuilder.DropColumn(
                name: "ModifiedUtc",
                table: "GSTRegistration");
        }
    }
}
