﻿using eSimulation.Framework;
using eSimulation.Framework.Domain;
using eSimulation.Framework.Service.Core;
using eSimulation.Helpers;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eSimulation.Areas.admin.Controllers
{
    [Area("admin")]
    public class PanController:Controller
        
      
    {
        private readonly eSimulationContext _context;
        private readonly IHelper _helper;
        private readonly IPanService _panService;
        public PanController(eSimulationContext eSimulation, IHelper helper, IPanService panService)
        {
            _context = eSimulation;
            _helper = helper;
            _panService = panService;
        }
        public IActionResult Index()
        {
            var DbContext = _panService.GetAllPan();
            return View(DbContext);
        }

        // GET: dashboard/Pan/Auto Generate
        public IActionResult Generate(Pannumber pannumber)
        {
            Function:
            var chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            var stringChars = new char[5];
           
            var random = new Random();
            var stringNumb = random.Next(1000, 9999);
            var lastchar = random.Next('A', 'Z');
            for (int i = 0; i < stringChars.Length; i++)
            {
                stringChars[i] = chars[random.Next(chars.Length)];
            }

            var finalString = new string(stringChars) + (stringNumb.ToString()) + Convert.ToChar(lastchar);
             pannumber.pan_no = finalString;
            
            if(_context.Pannumbers.Any(c=>c.pan_no == finalString)){
                goto Function;
            }
            else
            {
                _context.Pannumbers.Add(pannumber);
                _context.SaveChanges();
            }

            return RedirectToAction("Index","Pan");
        }
        [HttpGet]
        public IActionResult Create()
        {
            return View();
        }

            // GET: dashboard/Pan/Create
            [HttpPost]
        public IActionResult Create(Pannumber pannumber)
        {
            _context.Add(pannumber);
            _context.SaveChanges();
            return RedirectToAction("Index", "Pan");

        }









            // GET: dashboard/Pan/Delete
            public async Task<IActionResult> Delete(int id)
        {
            var pan = await _context.Pannumbers.FindAsync(id);
            _context.Pannumbers.Remove(pan);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

    }
}
