﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace eSimulation.Areas.admin.Controllers
{
    [Area("admin")]
    public class HomeController: Controller
       
    {

        public IActionResult Index()
        {
            if (!User.Identity.IsAuthenticated)
                
                return RedirectToAction("Login","Auth");
            else
                return Redirect("/");
        }
        public IActionResult Dashboard_admin()
        {
            return View();
        }

    }
}
