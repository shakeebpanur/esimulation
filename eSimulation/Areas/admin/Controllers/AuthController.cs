﻿using eSimulation.Framework.Service.Core;
using eSimulation.Framework.VIewModel;
using eSimulation.Helpers;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace eSimulation.Areas.admin.Controllers
{
    [Area("admin")]
    public class AuthController : Controller
    {
        private readonly ILogger<AuthController> _logger;
        private readonly IUserService _userService;
        private readonly IHelper _helper;

        public AuthController(ILogger<AuthController> logger, IUserService userService,  IHelper helper)
        {
            _logger = logger;
            _userService = userService;
            _helper = helper;
        }
        public IActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Login(LoginView _data, string ReturnUrl)
        {
            try
            {
                var returnURL = Request.Query["ReturnUrl"].ToString();
                var authProperties = new AuthenticationProperties();
                authProperties.IsPersistent = _data.Remember;

                var user = _userService.AdminLogin(_data.EmailAddress, _data.Password);
                if (user != null)
                {

                    var claims = new List<Claim>{
                        new Claim("UserId",user.UserId.ToString()),
                        new Claim(ClaimTypes.Name,user.Name),
                        new Claim(ClaimTypes.Email,user.EmailAddress),
                    };

                    //HttpContext.Session.SetObjectAsJson("AdminPermission", user.UserRole.Permissions);

                    var claimsIdentity = new ClaimsIdentity(claims,"admin");
                    await HttpContext.SignInAsync("admin", new ClaimsPrincipal(claimsIdentity), authProperties);
                    if (string.IsNullOrEmpty(ReturnUrl))
                        return RedirectToAction("Dashboard_admin", "Home");

                    return Redirect(ReturnUrl);
                }
                else
                {
                    ModelState.AddModelError("", "Username or password invalid!");
                    return View(_data);
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", "An error has occured while proccessing your request !");
                _logger.LogError(ex, "Login");
                return View(_data);
            }
        }

        public IActionResult Logout()
        {
            HttpContext.SignOutAsync("admin");
            return RedirectToAction("login", "auth", new { @area = "admin" });
        }
    }
}
