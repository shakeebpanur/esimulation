﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace eSimulation.Helpers
{
    public static class ExtensionMethods
    {

        public static DateTime ToIndianTime(this DateTime dateTime)
        {
            var localTime = new DateTime();
            if (dateTime.Kind == DateTimeKind.Unspecified || dateTime.Kind == DateTimeKind.Utc)
            {
                var zone = TimeZoneInfo.FindSystemTimeZoneById("India Standard Time");
                localTime = TimeZoneInfo.ConvertTimeFromUtc(dateTime, zone);
            }
            else
                localTime = dateTime;


            return localTime;
        }

    }
}
