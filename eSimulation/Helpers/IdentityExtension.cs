﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Security.Principal;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace eSimulation.Helpers
{
    public static class IdentityExtension 
    {
        public static int GetUserId(this IIdentity identity)
        {
            var _UserId = ((ClaimsIdentity)identity).FindFirst("UserId");
            return _UserId != null ? Convert.ToInt32(_UserId.Value) : 0;
        }
        public static string GetUserName(this IIdentity identity)
        {
            var _UserN = ((ClaimsIdentity)identity).FindFirst("name");
            var _UserName = _UserN != null ? _UserN.Value : "";
            return _UserName;
        }

        public static string GetEmailAddress(this IIdentity identity)
        {
            var _email = ((ClaimsIdentity)identity).FindFirst(ClaimTypes.Email);
            var emailAddress = _email != null ? _email.Value : "";
            return emailAddress;
        }
    }
}
