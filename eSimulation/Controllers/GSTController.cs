﻿using eSimulation.Framework.Service.Core;
using eSimulation.Framework;
using eSimulation.Framework.Enum;
using eSimulation.Framework.Domain;
using eSimulation.Helpers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Security.Claims;

namespace eSimulation.Controllers
{
    public class GSTController : Controller
    {
        private readonly IBasicService _basicService;
        private readonly IGSTService _gstService;
        private readonly eSimulationContext _context;




        public GSTController(IBasicService basicService, eSimulationContext eSimulationContext, IGSTService gstService)
        {
            _basicService = basicService;
            _context = eSimulationContext;
            _gstService = gstService;

        }
        public IActionResult Index()
        {
            
                return View();
        }

        public IActionResult New_registration()
        {
            ViewBag.CountryId = new SelectList(_basicService.GetCountries(), "CountryId", "Name");

            return View();
        }

      

        [HttpPost]

        public IActionResult New_registration(GSTRegistration gst)
        {

            if(gst.Pan_number != null)
            {
                var PanId = _context.Pannumbers.Where(c => c.pan_no == gst.Pan_number).FirstOrDefault();

               
                if(PanId != null)
                {
                    if (_gstService.CheckDuplicateGstPanReg(gst.UserId,PanId.Pan_Id) == 0)
                    {
                        gst.Pan_Id = PanId.Pan_Id;
                        gst.GstRegStatus = GstRegStatus.Draft;
                        gst.CreatedUtc = DateTime.UtcNow;
                        gst.ExpiryUtc = DateTime.UtcNow.AddDays(14);
                        gst.FormNumber = "GST REG- 01";
                        gst.FormDescription = "Application For new Registration";

                        _context.GSTRegistrations.Add(gst);
                        _context.SaveChanges();
                    }
                    else
                    {
                        TempData["Error"] = " PAN Already registered";
                        ViewBag.CountryId = new SelectList(_basicService.GetCountries(), "CountryId", "Name");
                        return View(gst);
                    }
                   
                }
                else
                {
                    TempData["Error"] = "Invalid PAN number";
                    ViewBag.CountryId = new SelectList(_basicService.GetCountries(), "CountryId", "Name");
                    return View(gst);
                }


            }
            else
            {
                TempData["Error"] = "Invalid PAN number";
                ViewBag.CountryId = new SelectList(_basicService.GetCountries(), "CountryId", "Name");
                return View(gst);

            }

            return RedirectToAction("New_registration_otp", "GST");
        }

        public IActionResult New_registration_otp(GSTRegistration gst)
        {
            return View();
        }
        
        [HttpPost]
        public IActionResult New_registration_otp1(GSTRegistration OtpSubmit)
        {
            if(OtpSubmit.Otp == "12345")
            {
                var gst = _context.GSTRegistrations.Where(c => c.UserId == OtpSubmit.UserId).FirstOrDefault();
                gst.GstRegStatus = GstRegStatus.Active;
                _context.Update(gst);
                _context.SaveChanges(); 
            }
            else
            {
                TempData["Error"] = "Incorrect OTP";
                return RedirectToAction("New_registration_otp", "GST");
            }

            return RedirectToAction("GstDashboard","GST");
        }

        public IActionResult GstDashboard()
        {
            var userId = User.Identity.GetUserId();
            var gst = _context.GSTRegistrations.Where(c => c.UserId == userId).ToList();
            return View(gst);
        }




        public IActionResult GetStates(int CountryId)
        {
            var _states = _basicService.GetStates(CountryId);
            return Ok(_states);
        }

        public IActionResult GetDistricts(int StateId)
        {
            var _dist = _basicService.GetDistrict(StateId);
            return Ok(_dist);
        }
    }
}
