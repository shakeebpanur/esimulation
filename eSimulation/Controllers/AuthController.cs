﻿using eSimulation.Framework.VIewModel;
using eSimulation.Framework.Service;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using eSimulation.Framework.Service.Core;
using System.Security.Claims;
using Microsoft.AspNetCore.Authentication.Cookies;
using eSimulation.Helpers;

namespace eSimulation.Controllers
{
    public class AuthController : Controller
    {
        private readonly ILogger<AuthController> _logger;
        private readonly IUserService _userService ;
        private readonly IHelper _helper;
        

        public AuthController(ILogger<AuthController> logger, IUserService userService, IHelper helper)
        {
            _logger = logger;
            _userService = userService;
            _helper = helper;
        }

        [HttpGet]
        public IActionResult Login()
        {
            if (!User.Identity.IsAuthenticated)
                return View();
            else
                return Redirect("/");
        }

        [HttpPost]
        public async Task<IActionResult> Login(LoginView _data, string ReturnUrl)
        {
            try
            {
                var returnURL = Request.Query["ReturnUrl"].ToString();
                var authProperties = new AuthenticationProperties();
                authProperties.IsPersistent = _data.Remember;

                var user = _userService.UserLogin(_data.EmailAddress, _data.Password);
                if (user != null)
                {

                    var claims = new List<Claim>{
                        new Claim("UserId",user.UserId.ToString()),
                        new Claim(ClaimTypes.Name,user.Name),
                        new Claim(ClaimTypes.Email,user.EmailAddress),

                       };

                        var claimsIdentity = new ClaimsIdentity(claims, CookieAuthenticationDefaults.AuthenticationScheme);
                        await HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, new ClaimsPrincipal(claimsIdentity), authProperties);
                        //HttpContext.Session.SetString("InstituteStatus", ((int)user.InstituteStatus).ToString());

                        if (string.IsNullOrEmpty(ReturnUrl))
                            return RedirectToAction("dashboard", "home");

                        return Redirect(ReturnUrl);
                    
                }
                else
                {
                    ModelState.AddModelError("", "Username or password invalid!");
                    return View("login", _data);
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", "An error has occured while proccessing your request !");
                _logger.LogError(ex, "Login");
                return View(_data);
            }
        }
        public IActionResult Logout()
        {
            HttpContext.SignOutAsync();
            return RedirectToAction("Login", "Auth");
        }



    }


}
