﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using eSimulation.Helpers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using eSimulation.Models;

namespace NergyCertificate.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly IHelper _helper;

        public HomeController(ILogger<HomeController> logger, IHelper helper)
        {
            _logger = logger;
            _helper = helper;
        }

        public IActionResult Index()
        {
            if (!User.Identity.IsAuthenticated)

                return RedirectToAction("Login", "Auth");
            else
                return Redirect("/");
        }

        public IActionResult Dashboard()
        {
            return View();
        }


        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

       


    }
}
